#ifndef THREADPOOL_H_INCLUDED
#define THREADPOOL_H_INCLUDED

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

struct ThreadPool
{
    ThreadPool(size_t n = 4);
    ~ThreadPool();

    bool add_job(const std::function<void()>& job);
    void wait_all_jobs_done();
    void join_all();
    void kill_all();

private:
    void runner();

    std::vector<std::thread> runners;
    bool _kill;
    bool _finish;

    std::atomic<size_t> running;
    std::condition_variable running_cv;
    std::mutex running_mtx;

    std::queue<std::function<void()>> jobs;
    std::condition_variable jobs_cv;
    std::mutex jobs_mtx;
};

#endif /* THREADPOOL_H_INCLUDED */
