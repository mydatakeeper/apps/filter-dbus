#ifndef FILTER_LIST_H_INCLUDED
#define FILTER_LIST_H_INCLUDED

#include <string>
#include <vector>
#include <mutex>

#include <ad-block/ad_block_client.h>
#include <dbus-c++/dbus.h>
#include <json/json.hh>

struct FilterList
{
    using DBusStruct = DBus::Struct<
        std::string,
        std::string,
        std::string,
        std::string,
        std::vector<std::string>,
        bool,
        std::map<std::string, std::string>,
        uint64_t,
        uint64_t,
        uint32_t
    >;

    FilterList(const JSON::Object& json);
    FilterList(const DBusStruct& dbus);
    FilterList(
        const JSON::Value uuid,
        const JSON::Value title,
        const JSON::Value desc,
        const JSON::Value url,
        const JSON::Array langs,
        const JSON::Value enabled,
        const JSON::Object meta
    );
    FilterList(
        const std::string uuid,
        const std::string title,
        const std::string desc,
        const std::string url,
        const std::vector<std::string> langs,
        bool enabled,
        const std::map<std::string, std::string> meta,
        uint64_t last_update = 0,
        uint64_t last_modified = 0,
        uint32_t update_frequency = 0
    );
    ~FilterList();

    operator DBusStruct() const;

    void reset();

    uint64_t load(const std::string& cache_folder);
    uint64_t update(const std::string& cache_folder);

    bool outdated() const;
    uint64_t next_update() const;

    bool locale_enabled(const std::string& locale) const;

    // Wrap client methods
    bool matches(
        const char* input,
        FilterOption contextOption = FONoFilterOption,
        const char* contextDomain = nullptr,
        Filter** matchedFilter = nullptr,
        Filter** matchedExceptionFilter = nullptr);

    const std::string uuid;
    const std::string title;
    const std::string desc;
    const std::string url;
    const std::vector<std::string> langs;
    bool enabled;
    const std::map<std::string, std::string> meta;

    uint64_t last_update;
    uint64_t last_modified;
    uint32_t update_frequency;

    AdBlockClient client;
private:
    enum {
        UNINITIALIZED,
        LOADED,
        DOWNLOADED,
    } state;

    std::mutex mtx;

    char* serialize_buffer;
};

#endif /* FILTER_LIST_H_INCLUDED */
