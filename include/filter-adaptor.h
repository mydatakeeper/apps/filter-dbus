#ifndef FILTER_ADAPTOR_H_INCLUDED
#define FILTER_ADAPTOR_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "fr.mydatakeeper.filter.h"
#include "filter-list.h"
#include "threadpool.h"

extern const std::string FilterPath;

class FilterAdaptor
: public DBusAdaptor,
  public fr::mydatakeeper::filter_adaptor
{
public:
    FilterAdaptor(
        DBus::Connection &connection,
        const vector<string> locales,
        const std::string &filter_list_filename,
        const std::string &cache_folder,
        size_t thread_count = 8);
    virtual ~FilterAdaptor() {}

    virtual std::map<std::string, std::vector<FilterList::DBusStruct>> get_filter_list();
    virtual bool add_filter_list(const FilterList::DBusStruct& dbus);
    virtual bool remove_filter_list(const std::string& uuid);
    virtual bool enable_filter_list(const std::string& category, const std::string& uuid);
    virtual bool disable_filter_list(const std::string& category, const std::string& uuid);

    virtual uint64_t update_filter_lists();

    virtual bool filter_domain_name(const std::string& domain_name);
    virtual bool filter_url(
        const std::string& url,
        const std::string& origin,
        const std::string& mimetype);

    void properties_changed(const std::vector<std::string>& properties);

    void interrupt_lists_update();

    void update_locale(const vector<string> locales);

private:
    uint64_t update_filter_list(FilterList& list);

    std::map<std::string, std::list<FilterList>> filter_lists;
    ThreadPool thread_pool;
    const std::string &cache_folder;
    std::string locale;
};

#endif /* FILTER_ADAPTOR_H_INCLUDED */
