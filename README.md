
# Filter Dbus

## Presentation

This project aims to provides a Dbus interface for domain and URL filtering based on lists.

It currently handles:

 * Adding/Removing/Enabling/Disabling lists.
 * Filter domain name and URLs (based on origin).
 * Automatic download and activation of lists based on system locale.

## Usage

### Prerequisites

You will need:

 * A modern C/C++ compiler
 * CMake 3.1+ installed

### Building The Project

```shell_session
$ git clone https://gitlab.com/mydatakeeper/apps/filter-dbus.git
$ cd filter-dbus
$ mkdir build
$ cd build
$ cmake ..
$ make -j8
```

### Installing the project

```shell_session
# make install
```

## Project Structure

There are three folders: `src`, `include`, and `dbus-desc`. Each folder serves a self-explanatory purpose.

Source files are in `src`. Header files are in `include`. Dbus description files in XML format are in `dbus-desc`.

## Contributing

**Merge Requests are WELCOME!** Please submit any fixes or improvements, and I promise to review it as soon as I can at the project URL:

 * [Project Gitlab Home](https://gitlab.com/mydatakeeper/apps/filter-dbus)
 * [Submit Issues](https://gitlab.com/mydatakeeper/apps/filter-dbus/-/issues)
 * [Merge Requests](https://gitlab.com/mydatakeeper/apps/filter-dbus/-/merge_requests)

## License

&copy; 2019-2020 Mydatakeeper S.A.S.

Open sourced under GPLv3 license. See attached LICENSE file.
