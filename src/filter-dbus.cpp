#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>
#include <csignal>

#include <dbus-c++/dbus.h>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/systemd.h>

#include "filter-adaptor.h"

const string FilterName("fr.mydatakeeper.filter");

using namespace std;

DBus::BusDispatcher dispatcher;

condition_variable update_cond;
atomic<bool> update_exit;

void update(auto& filter)
{
    while (!update_exit.load()) {
        uint64_t now = chrono::duration_cast<chrono::seconds>(
            chrono::system_clock::now().time_since_epoch()).count();

        clog << "Updating Filter values" << endl;
        filter.LastUpdate = now;
        uint64_t requested_update = filter.update_filter_lists();
        if (!requested_update) {
            filter.LastUpdateSuccessful = false;
        } else {
            // If a list expires before the next update adjust timer
            filter.LastUpdateSuccessful = true;
        }
        filter.properties_changed({"LastUpdate", "LastUpdateSuccessful"});

        // Waiting for next update
        int32_t timeout = filter.LastUpdateSuccessful() ?
            filter.UpdateTimeout() : filter.RetryTimeout();
        uint64_t next_update = filter.LastUpdate() + timeout;
        if (requested_update)
            next_update = min(next_update, requested_update);
        if (now < next_update) {
            uint32_t sleep = next_update - now;
            clog << "Next update in " << sleep << " seconds" << endl;
            mutex mtx;
            unique_lock<mutex> lck(mtx);
            update_cond.wait_for(lck, chrono::seconds(sleep));
            continue;
        }
    }
    clog << "Exiting Filter update thread" << endl;
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

void signal_handler(int sig)
{
    clog << strsignal(sig) << " signal received" << endl;
    dispatcher.leave();
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, filter_list_filename, cache_folder, thread_count;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used for Dbus object. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, FilterName, nullptr,
            "The Dbus name of the Filter"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, FilterPath, nullptr,
            "The Dbus path of the Filter"
        },
        {
            filter_list_filename, "filter-list", 0, mydatakeeper::required_argument, "/usr/share/filter-dbus/list.json", nullptr,
            "A Json file describing a list of ad-blocking lists"
        },
        {
            cache_folder, "cache-folder", 0, mydatakeeper::required_argument, "/var/cache/filter-dbus", nullptr,
            "A cache folder used for list files"
        },
        {
            thread_count, "thread-count", 0, mydatakeeper::required_argument, "8", nullptr,
            "The number of running threads"
        },
    });

    // Setup signal handler
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    clog << "Getting Localed proxy" << endl;
    org::freedesktop::locale1Proxy localed(conn);

    clog << "Setting up Filter proxy" << endl;
    FilterAdaptor filter(
        conn,
        (vector<string>)localed.Get("org.freedesktop.locale1", "Locale"),
        filter_list_filename,
        cache_folder,
        atoi(thread_count.c_str()));
    filter.RetryTimeout = 60;
    filter.UpdateTimeout = 60*60*6;
    filter.LastUpdate = 0;
    filter.LastUpdateSuccessful = false;

    filter.onSet = [&filter](auto& /* iface */, auto& name, auto& value) {
        if (name == "RetryTimeout") {
            filter.RetryTimeout = value.operator uint32_t();
            if (!filter.LastUpdateSuccessful())
                update_cond.notify_one();
            filter.properties_changed({name});
        }
        if (name == "UpdateTimeout") {
            filter.UpdateTimeout = value.operator uint32_t();
            if (filter.LastUpdateSuccessful())
                update_cond.notify_one();
            filter.properties_changed({name});
        }
    };

    clog << "Setting up localed callback" << endl;
    localed.onPropertiesChanged = [&filter](auto& iface, auto& changed, auto& invalidated)
    {
        if (changed.find("Locale") != changed.end()) {
            filter.update_locale(changed.at("Locale"));
        }
    };

    clog << "Starting update thread" << endl;
    thread update_thread([&filter]() { update(filter); });

    clog << "Requesting DBus name" << endl;
    conn.request_name(bus_name.c_str());

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    clog << "Waiting update thread to exit" << endl;
    update_exit.store(true);
    filter.interrupt_lists_update();
    update_cond.notify_one();
    update_thread.join();

    return 0;
}