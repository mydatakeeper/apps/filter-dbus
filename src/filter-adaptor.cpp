#include "filter-adaptor.h"

#include <filesystem>
#include <fstream>
#include <mutex>

extern const string FilterPath("/fr/mydatakeeper/filter");

using namespace std::filesystem;

namespace {
std::mutex mutex_io;

string extract_locale(const vector<string>& locales) {
    for (auto& str : locales) {
        if (str.rfind("LANG=", 0) == 0) {
            return string(str, 5, str.find('_', 5) - 5);
        } else if (str.rfind("LANGUAGE=", 0) == 0) {
            return string (str, 9);
        }
    }
    return {};
}

#ifdef DEBUG
void logMatch(auto& category, auto& list, auto& matchedFilter, auto& matchedExceptionFilter) {
    std::unique_lock lck(mutex_io);
    if (matchedFilter)
        clog << "Matched: " << string(matchedFilter->data, matchedFilter->dataLen) << endl;
    if (matchedExceptionFilter)
        clog << "Exception: " << string(matchedExceptionFilter->data, matchedExceptionFilter->dataLen) << endl;
    clog << "\tCategory: " << category << endl;
    clog << "\tList: " << endl;
    clog << "\t\tUUID: " << list.uuid << endl;
    clog << "\t\tURL: " << list.url << endl;
    clog << "\t\tTitle: " << list.title << endl;
    clog << "\t\tDescription: " << list.desc << endl;
}
#else /* DEBUG */
void logMatch(auto&, auto&, auto&, auto&) {}
#endif /* DEBUG */
} /* End of namespace */

FilterAdaptor::FilterAdaptor(
    DBus::Connection &connection,
    const vector<string> locales,
    const string &filter_list_filename,
    const string &cache_folder,
    size_t thread_count)
: DBusAdaptor(connection, FilterPath)
, thread_pool(thread_count)
, cache_folder(cache_folder)
, locale(extract_locale(locales))
{
    // Don't test return value. Will return false if folder already exists
    create_directories(cache_folder);

    const JSON::Object json = parse_file(filter_list_filename.c_str());
    for (const auto& it: json)
    {
        auto& category = filter_lists[it.first];
        const JSON::Array &arr = it.second;
        for (const auto& list : arr) {
            category.emplace_back((const JSON::Object&)list);
        }
    }
    filter_lists["custom"];
}

void FilterAdaptor::properties_changed(const vector<string>& properties)
{
    map<string, DBus::Variant> changed;
    for(auto& prop : properties)
        changed[prop] = *fr::mydatakeeper::filter_adaptor::get_property(prop);
    PropertiesChanged(fr::mydatakeeper::filter_adaptor::name(), changed, {});
}

uint64_t FilterAdaptor::update_filter_lists()
{
    clog << "Updating Filter Lists" << endl;
    vector<uint64_t> next_updates;
    for (auto& it : filter_lists) {
        for (auto& list : it.second) {
            thread_pool.add_job([&next_updates, &list, this]() {
                next_updates.push_back(update_filter_list(list));
            });
        }
    }
    thread_pool.wait_all_jobs_done();
    return *min_element(next_updates.begin(), next_updates.end());
}

uint64_t FilterAdaptor::update_filter_list(FilterList& list)
{
    if (!list.enabled || !list.locale_enabled(locale)) {
#ifdef DEBUG
        clog << "List " << list.title << " is disabled" << endl;
#endif /* DEBUG */
        list.reset();
        return std::numeric_limits<uint64_t>::max();
    }

    if (list.outdated())
        return list.update(cache_folder);

    clog << "List " << list.title << " is up to date" << endl;
    return list.next_update();
}

std::map<std::string, std::vector<FilterList::DBusStruct>> FilterAdaptor::get_filter_list()
{
    std::map<std::string, std::vector<FilterList::DBusStruct>> ret;    
    for (auto& it : filter_lists) {
        auto &category = ret[it.first];
        for (auto& list : it.second) {
            category.emplace_back(list);
        }
    }

    return ret;
}

bool FilterAdaptor::add_filter_list(const FilterList::DBusStruct& dbus)
{
    const auto& uuid = dbus._1;
    auto& custom = filter_lists["custom"];
    auto search = find_if(custom.begin(), custom.end(), [&uuid](auto& it){ return it.uuid == uuid; });
    if (search != custom.end())
        return false;
    
    custom.emplace_back(dbus);
    if (!update_filter_list(custom.back())) {
        custom.pop_back();
        return false;
    }
    return true;
}

bool FilterAdaptor::remove_filter_list(const std::string& uuid)
{
    auto& custom = filter_lists["custom"];
    auto list = find_if(custom.begin(), custom.end(), [&uuid](auto& it){ return it.uuid == uuid; });
    if (list == custom.end())
        return false;

    custom.erase(list);
    return true;
}

bool FilterAdaptor::enable_filter_list(const std::string& cat, const std::string& uuid)
{
    auto category = filter_lists.find(cat);
    if (category == filter_lists.end())
        return false;
    auto& lists = category->second;
    auto list = find_if(lists.begin(), lists.end(), [&uuid](auto& it){ return it.uuid == uuid; });
    if (list == lists.end())
        return false;

    list->enabled = true;
    return true;
}

bool FilterAdaptor::disable_filter_list(const std::string& cat, const std::string& uuid)
{
    auto category = filter_lists.find(cat);
    if (category == filter_lists.end())
        return false;
    auto& lists = category->second;
    auto list = find_if(lists.begin(), lists.end(), [&uuid](auto& it){ return it.uuid == uuid; });
    if (list == lists.end())
        return false;

    list->enabled = false;
    return true;
}

bool FilterAdaptor::filter_domain_name(const string& domain_name)
{
    bool matched = false;
#ifdef DEBUG
    clog << "Checking domain name " << domain_name << endl;
#endif /* DEBUG */
    for (auto& it : filter_lists) {
        for (auto& list : it.second) {
            if (!list.enabled || !list.locale_enabled(locale)) {
                continue;
            }
            // Prepend protocol to bypass protocol check
            Filter* matchedFilter = nullptr;
            Filter* matchedExceptionFilter = nullptr;
            const string url = string("http://") + domain_name;
            bool ret = list.matches(
                url.c_str(),
                FODomainOnly,
                url.c_str(),
                &matchedFilter,
                &matchedExceptionFilter
            );
            if (ret) {
                logMatch(it.first, list, matchedFilter, matchedExceptionFilter);
                matched = true;
            } else {
#ifdef DEBUG
                clog << "No match in list " << list.title << endl;
#endif /* DEBUG */
            }
        }
    }
#ifdef DEBUG
    if (!matched)
        clog << "No match" << endl;
#endif /* DEBUG */
    return matched;
}

bool FilterAdaptor::filter_url(const string& url, const string& origin, const string& mimetype)
{
    bool matched = false;
#ifdef DEBUG
    clog << "Checking url " << url << " with origin " << origin << endl;
#endif /* DEBUG */
    /* From https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types */
    static const vector<string> scriptMimeTypes = {
        "application/javascript",
        "application/ecmascript",
        "application/x-ecmascript",
        "application/x-javascript",
        "text/javascript",
        "text/ecmascript",
        "text/javascript1.0",
        "text/javascript1.1",
        "text/javascript1.2",
        "text/javascript1.3",
        "text/javascript1.4",
        "text/javascript1.5",
        "text/jscript",
        "text/livescript",
        "text/x-ecmascript",
        "text/x-javascript",
    };
    static const vector<string> imageMimeTypes = {
        "image/apng",
        "image/bmp",
        "image/gif",
        "image/x-icon",
        "image/jpeg",
        "image/png",
        "image/svg+xml",
        "image/tiff",
        "image/webp",
    };
    static const vector<string> cssMimeTypes = {
        "text/css",
    };

    FilterOption opt = FONoFilterOption;
    if (find(scriptMimeTypes.begin(), scriptMimeTypes.end(), mimetype) != scriptMimeTypes.end())
        opt = static_cast<FilterOption>(opt | FOScript);
    else if (find(imageMimeTypes.begin(), imageMimeTypes.end(), mimetype) != imageMimeTypes.end())
        opt = static_cast<FilterOption>(opt | FOImage);
    else if (find(cssMimeTypes.begin(), cssMimeTypes.end(), mimetype) != cssMimeTypes.end())
        opt = static_cast<FilterOption>(opt | FOStylesheet);

    // TODO: support more FilterOption flags
    // TODO: add locale support

    for (auto& it : filter_lists) {
        for (auto& list : it.second) {
            if (!list.enabled || !list.locale_enabled(locale)) {
                continue;
            }
            Filter* matchedFilter = nullptr;
            Filter* matchedExceptionFilter = nullptr;
            bool ret = list.matches(
                url.c_str(),
                opt,
                origin.c_str(),
                &matchedFilter,
                &matchedExceptionFilter
            );
            if (ret) {
                logMatch(it.first, list, matchedFilter, matchedExceptionFilter);
                matched = true;
            } else {
#ifdef DEBUG
                clog << "No match in list " << list.title << endl;
#endif /* DEBUG */
            }
        }
    }
#ifdef DEBUG
    if (!matched)
        clog << "No match" << endl;
#endif /* DEBUG */
    return matched;
}

void FilterAdaptor::interrupt_lists_update()
{
    thread_pool.kill_all();
}

void FilterAdaptor::update_locale(const vector<string> locales)
{
    locale = extract_locale(locales);
    update_filter_lists();
}
