#include "threadpool.h"

#include <iostream>

namespace {
std::mutex mutex_io;
} /* End of namespace */

ThreadPool::ThreadPool(size_t n)
: _kill(false)
, _finish(false)
, running(0)
{
    std::clog << "Starting " << n << " runners" << std::endl;
    for (size_t i = 0; i < n; ++i)
        runners.emplace_back(std::bind(&ThreadPool::runner, this));
}

ThreadPool::~ThreadPool()
{
    kill_all();
}

bool ThreadPool::add_job(const std::function<void()>& job)
{
    if (_kill) {
        return false;
    }

    jobs.push(job);
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] Added a job" << std::endl;
    }
#endif /* DEBUG */
    jobs_cv.notify_one();
    return true;
}

void ThreadPool::wait_all_jobs_done()
{
    std::unique_lock<std::mutex> lck(running_mtx);
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] Waiting for all jobs to be done" << std::endl;
    }
#endif /* DEBUG */
    running_cv.wait(lck, [this] { return running.load() == 0 && jobs.empty(); });
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] All jobs done" << std::endl;
    }
#endif /* DEBUG */
}

void ThreadPool::join_all()
{
    _finish = true;
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] Joining all runners" << std::endl;
    }
#endif /* DEBUG */
    jobs_cv.notify_all();
    for (auto& t : runners)
        t.join();
    runners.clear();
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] All runners joined" << std::endl;
    }
#endif /* DEBUG */
}

void ThreadPool::kill_all()
{
    _kill = true;
    // Empty job queue
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] Dropping leftover jobs" << std::endl;
    }
#endif /* DEBUG */
    while (!jobs.empty()) jobs.pop();

#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] Joining all runners" << std::endl;
    }
#endif /* DEBUG */
    jobs_cv.notify_all();
    for (auto& t : runners)
        t.join();
    runners.clear();
#ifdef DEBUG
    {
        std::unique_lock lck(mutex_io);
        std::clog << "[" << jobs.size() << "," << running << "] All runners joined" << std::endl;
    }
#endif /* DEBUG */
}

void ThreadPool::runner()
{
    while (true) {
        std::function<void()> job;
        {
            std::unique_lock<std::mutex> lck(jobs_mtx);
            jobs_cv.wait(lck, [this] {
                return !jobs.empty() || _kill || _finish;
            });
            if (_kill) {
                return;
            }

            if (jobs.empty()) {
                if (_finish) {
                    return;
                }
                continue;
            }

            job = jobs.front();
            jobs.pop();
        }
        running++;
#ifdef DEBUG
        {
            std::unique_lock lck(mutex_io);
            std::clog << "[" << jobs.size() << "," << running << "] Starting job" << std::endl;
        }
#endif /* DEBUG */
        running_cv.notify_all();
        job();
        running--;
#ifdef DEBUG
        {
            std::unique_lock lck(mutex_io);
            std::clog << "[" << jobs.size() << "," << running << "] Job done" << std::endl;
        }
#endif /* DEBUG */
        running_cv.notify_all();
    }
}
