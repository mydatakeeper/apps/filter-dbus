#include "filter-list.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <limits>
#include <sys/stat.h>

#include <mydatakeeper/exec.h>
#include <mydatakeeper/download.h>

namespace {
uint32_t extract_update_frequency(const std::string &buffer)
{
    static const std::string expires("! Expires: ");
    size_t idx = buffer.find(expires);
    if (idx == std::string::npos) {
        return 0;
    }

    idx += expires.size();
    std::string expiry_time = buffer.substr(idx, buffer.find('\n', idx) - idx);

    /* Extract update frequency */
    uint32_t duration = stoi(expiry_time);
    if (expiry_time.find("day") != std::string::npos) duration *= 60*60*24;
    else if (expiry_time.find("d") != std::string::npos) duration *= 60*60*24;
    else if (expiry_time.find("hour") != std::string::npos) duration *= 60*60;
    else if (expiry_time.find("h") != std::string::npos) duration *= 60*60;
    else if (expiry_time.find("minute") != std::string::npos) duration *= 60;
    else if (expiry_time.find("m") != std::string::npos) duration *= 60;

    return duration;
}

auto ifind(const std::string& haystack, const std::string& needle) -> decltype(haystack.begin())
{
    return std::search(
        haystack.begin(), haystack.end(),
        needle.begin(),   needle.end(),
        [](char c1, char c2) { return std::toupper(c1) == std::toupper(c2); }
    );
}

uint64_t extract_last_modified(const std::string &buffer)
{
    static const std::string last_modified("! Last modified: ");
    auto it = ifind(buffer, last_modified);
    if (it == buffer.end()) {
        return std::numeric_limits<uint64_t>::min();
    }
    it += last_modified.length();

    /* Extract last modified time */
    std::string modified_time(it, find(it, buffer.end(), '\n'));
    std::replace(modified_time.begin(), modified_time.end(), '-', '/');

    tm t;
    /* Specific formats*/
    if (strptime(modified_time.c_str(), "%Y %B %d, %H:%M:%S", &t) != NULL) {
        const time_t gmttime = timegm(&t);
        return (uint64_t)gmttime;
    }
    if (strptime(modified_time.c_str(), "%d/%m/%Y", &t) != NULL) {
        const time_t gmttime = timegm(&t);
        return (uint64_t)gmttime;
    }

    /* Remove confusing format extra chars */
    size_t i;
    if ((i = modified_time.find("st of")) != std::string::npos) modified_time.erase(i, 5);
    if ((i = modified_time.find("nd of")) != std::string::npos) modified_time.erase(i, 5);
    if ((i = modified_time.find("rd of")) != std::string::npos) modified_time.erase(i, 5);
    if ((i = modified_time.find("th of")) != std::string::npos) modified_time.erase(i, 5);

    std::string timestamp, err;
    if (execute("/usr/bin/date", {"--date", modified_time, "+%s"}, &timestamp, &err) != 0) {
        cerr << err << endl;
        return std::numeric_limits<uint64_t>::min();
    }

    timestamp.erase(--timestamp.end());
    return atoi(timestamp.c_str());
}

#ifdef DEBUG
void logFile(auto& name, auto& update_frequency, auto& last_modified, auto& last_update) {
    clog << "List name: " << name << endl;
    clog << "\tUpdate frequency: " << update_frequency << endl;
    clog << "\tLast modified: " << last_modified << endl;
    clog << "\tLast update: " << last_update << endl;
}
#else
void logMatch(auto&, auto&, auto&, auto&) {}
#endif /* DEBUG */

} /* end namespace */


FilterList::FilterList(const JSON::Object& json)
:   FilterList(
        json["uuid"],
        json["title"],
        json["desc"],
        json["url"],
        json["langs"],
        json["enabled"],
        json["meta"]
    )
{}

FilterList::FilterList(const DBusStruct& dbus)
:   FilterList(
        dbus._1,
        dbus._2,
        dbus._3,
        dbus._4,
        dbus._5,
        dbus._6,
        dbus._7,
        dbus._8,
        dbus._9,
        dbus._10
    )
{}


FilterList::FilterList(
    const JSON::Value uuid,
    const JSON::Value title,
    const JSON::Value desc,
    const JSON::Value url,
    const JSON::Array langs,
    const JSON::Value enabled,
    const JSON::Object meta)
:   FilterList(
        uuid.as_string(),
        title.as_string(),
        desc.as_string(),
        url.as_string(),
        std::vector<std::string>(langs.begin(), langs.end()),
        enabled.as_bool(),
        std::map<std::string, std::string>(meta.begin(), meta.end())
    )
{}

FilterList::FilterList(
        const std::string uuid,
        const std::string title,
        const std::string desc,
        const std::string url,
        const std::vector<std::string> langs,
        bool enabled,
        const std::map<std::string, std::string> meta,
        uint64_t last_update,
        uint64_t last_modified,
        uint32_t update_frequency
    )
:   uuid(uuid)
,   title(title)
,   desc(desc)
,   url(url)
,   langs(langs.begin(), langs.end())
,   enabled(enabled)
,   meta(meta.begin(), meta.end())

,   last_update(last_update)
,   last_modified(last_modified)
,   update_frequency(update_frequency)

,   client()

,   state(UNINITIALIZED)
,   serialize_buffer(nullptr)
{}

FilterList::~FilterList()
{
    if (serialize_buffer) {
        delete[] serialize_buffer;
        serialize_buffer = nullptr;
    }
}

FilterList::operator DBusStruct() const
{
    return {
        uuid,
        title,
        desc,
        url,
        langs,
        enabled,
        meta,

        last_update,
        last_modified,
        update_frequency
    };
}

void FilterList::reset()
{
    client.clear();
    if (serialize_buffer) {
        delete[] serialize_buffer;
        serialize_buffer = nullptr;
    }
    update_frequency = 0;
    last_modified = 0;
    last_update = 0;
    state = UNINITIALIZED;
}

uint64_t FilterList::load(const std::string& cache_folder)
{
    // Open binary file
    const std::string bin_filename = cache_folder + '/' + uuid + ".bin";
    std::ifstream ifs(bin_filename, ifstream::in);
    if (!ifs) { return 0; }

    // Cleanup
    reset();

    // Read update data
    std::unique_lock<std::mutex> lck(mtx);
    clog << "Loading " << title << " from binary cache (" << bin_filename << ')' << endl;
    ifs >> update_frequency; assert(ifs.peek() == ','); ifs.get();
    ifs >> last_modified; assert(ifs.peek() == ','); ifs.get();
    ifs >> last_update; assert(ifs.peek() == ','); ifs.get();

#ifdef DEBUG
    logFile(title, update_frequency, last_modified, last_update);
#endif /* DEBUG */

    // Read serialized data
    auto pos = ifs.tellg();
    ifs.seekg (0, ifs.end);
    auto len = ifs.tellg();
    ifs.seekg (pos, ifs.beg);
    auto size = len - pos;
    serialize_buffer = new char[size];
    ifs.read(serialize_buffer, size);

    // Deserialize file
    if (!client.deserialize(serialize_buffer)) {
        reset();
        return 0;
    }

    state = LOADED;
    return next_update();
}

uint64_t FilterList::update(const std::string& cache_folder)
{
    // Try loading it first
    if (state == UNINITIALIZED) {
        uint64_t next_update = load(cache_folder);
        if (next_update) return next_update;
    }

    // Download file
    string file_buffer;
    string err;
    clog << "Downloading " << title << " from URL (" << url << ')' << endl;
    if (!download_file(url, file_buffer, &err)) {
        cerr << err << endl;
        return 0;
    }

    // Cleanup
    reset();

    // Extract update data
    std::unique_lock<std::mutex> lck(mtx);
    update_frequency = extract_update_frequency(file_buffer);
    last_modified = extract_last_modified(file_buffer);
    last_update = chrono::duration_cast<chrono::seconds>(
        chrono::system_clock::now().time_since_epoch()).count();

#ifdef DEBUG
    logFile(title, update_frequency, last_modified, last_update);
#endif /* DEBUG */

    // Parse file
    if (!client.parse(file_buffer.c_str())) {
        reset();
        return 0;
    }

    // Serialize file
    int size;
    serialize_buffer = client.serialize(&size, false, false);
    if (serialize_buffer == nullptr || size == 0) { return 0; }

    // Open temporary file
    const std::string tmp_filename = cache_folder + '/' + uuid + ".tmp";
    std::ofstream ofs(tmp_filename, ofstream::out);
    if (!ofs) { return 0; }

    // Write binary file
    ofs << update_frequency << ',';
    ofs << last_modified << ',';
    ofs << last_update << ',';
    ofs.write(serialize_buffer, size);

    // Cleanup
    ofs.close();
    delete serialize_buffer;
    serialize_buffer = nullptr;

    // Move temporary file
    std::error_code err_code;
    const std::string bin_filename = cache_folder + '/' + uuid + ".bin";
    std::filesystem::rename(tmp_filename, bin_filename, err_code);
    if (err_code) {
        cerr << err_code << endl;
        return 0;
    }

    state = DOWNLOADED;
    return next_update();
}

bool FilterList::outdated() const
{
    uint64_t now = chrono::duration_cast<chrono::seconds>(
        chrono::system_clock::now().time_since_epoch()).count();
    return next_update() <= now;
}

uint64_t FilterList::next_update() const
{
    if (state == UNINITIALIZED)
        return 0;

    if (!update_frequency)
        return std::numeric_limits<uint64_t>::max();

    uint64_t now = chrono::duration_cast<chrono::seconds>(
        chrono::system_clock::now().time_since_epoch()).count();
    uint64_t next_update =
        (last_modified ? last_modified : last_update) + update_frequency;
    return (next_update > now) ?
        next_update : std::numeric_limits<uint64_t>::max();
}

bool FilterList::locale_enabled(const string& locale) const
{
    if (langs.empty()) {
        return true;
    }

    auto it = find(langs.begin(), langs.end(), locale);
    if (it != langs.end()) {
        return true;
    }

#ifdef DEBUG
    clog << "List " << title << " does not support locale " << locale << endl;
#endif /* DEBUG */
    return false;
}

bool FilterList::matches(
    const char* input,
    FilterOption contextOption,
    const char* contextDomain,
    Filter** matchedFilter,
    Filter** matchedExceptionFilter)
{
    std::unique_lock<std::mutex> lck(mtx);

    return client.matches(
        input,
        contextOption,
        contextDomain,
        matchedFilter,
        matchedExceptionFilter);
}
